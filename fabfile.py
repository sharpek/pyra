# -*- encoding: utf-8 -*-
from fabric.api import run, cd
from fabric.api import env
from fabric.context_managers import prefix


env.shell = '/usr/local/bin/bash -l -c'
env.roledefs = {
    'production': ['pyra@web2.mydevil.net']
}
env.path = '/home/pyra/sites/pyra/pyra'


def deploy():
    with prefix('workon pyra'):
        with cd('~/sites/pyra'):
            run('git pull')
            run('git reset --hard')
            run('pip install -q -r requirements/production.txt')
            with cd(env.path):
                run('./manage.py collectstatic --settings=pyra.settings.production --link --noinput')
                run('./manage.py syncdb --settings=pyra.settings.production --noinput')
                run('./manage.py migrate --settings=pyra.settings.production --noinput')
            run('touch /home/pyra/domains/pyra.mydevil.net/uwsgi0.sock')

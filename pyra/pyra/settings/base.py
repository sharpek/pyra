# -*- encoding: utf-8 -*-
import os
from django.conf import global_settings
from django.core.exceptions import ImproperlyConfigured


def get_env_var(name, **kwargs):
    try:
        return os.environ[name]
    except KeyError:
        if 'default' in kwargs:
            return kwargs['default']
        raise ImproperlyConfigured('Set the {} environment variable.'.format(name))


PROJECT_ROOT = os.path.dirname(os.path.dirname(__file__))

ADMINS = tuple()

MANAGERS = ADMINS

# TODO - zmienic gdy bedziemy znali domene
ALLOWED_HOSTS = ['*']

TIME_ZONE = 'Europe/Warsaw'

LANGUAGE_CODE = 'pl'

SITE_ID = 1

USE_I18N = True

USE_L10N = False

USE_TZ = False

STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
LOCAL_STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(STATIC_ROOT, 'upload')
MEDIA_URL = STATIC_URL + 'upload/'

STATICFILES_DIRS = tuple()

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SECRET_KEY = get_env_var('SECRET_KEY', default='secret')

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': get_env_var('CACHE_LOCATION',
                                default='127.0.0.1:11211'),
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
ROOT_URLCONF = 'pyra.urls'

WSGI_APPLICATION = 'pyra.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates'),
)

INSTALLED_APPS = (
    'grappelli',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',

    'south',
    'django_extensions',

    'apps.website',
    'apps.news',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'django.core.context_processors.request',
)

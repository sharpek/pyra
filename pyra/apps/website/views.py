# -*- encoding: utf-8 -*-
from annoying.decorators import render_to

from apps.news.models import News


@render_to('website/home.html')
def home(request):
    return {
        'news_list': News.get_latest_news()
    }

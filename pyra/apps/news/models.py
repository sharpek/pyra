# -*- encoding: utf-8 -*-
import datetime

from django.db import models
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse

from model_utils import Choices
from model_utils.fields import StatusField
from model_utils.models import TimeStampedModel

from django_extensions.db.fields import AutoSlugField


User = get_user_model()


class News(TimeStampedModel):
    STATUS = Choices('draft', 'published')

    title = models.CharField(u'Tytuł', max_length=256)
    slug = AutoSlugField(populate_from='title')
    date = models.DateTimeField(u'Data publikacji',
                                default=datetime.datetime.now)
    author = models.ForeignKey(User, verbose_name='Autor')
    precontent = models.TextField(u'Zajawka', help_text=u'Wstęp do newsa',
                                  blank=True)
    content = models.TextField(u'Treść', blank=True)
    status = StatusField()

    class Meta:
        verbose_name = 'news'
        verbose_name_plural = 'news'
        ordering = ['-date']
        get_latest_by = 'date'

    def __unicode__(self):
        return self.title

    @classmethod
    def queryset(cls):
        return cls.objects.select_related('user')

    @classmethod
    def get_latest_news(cls):
        return cls.queryset().filter(status=cls.STATUS.published)[0:3]

    def get_absolute_url(self):
        return reverse('news:detail', args=[self.pk, self.slug])

# -*- encoding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'apps.news.views.index', name='index'),
    url(r'^(?P<pk>\d+)/(?P<slug>[a-zA-Z0-9_,.-]+)\.html$',
        'apps.news.views.detail', name='detail')
)

# -*- encoding: utf-8 -*-
from django.shortcuts import (
    get_object_or_404,
    redirect
)

from annoying.decorators import render_to

from .models import News


@render_to('news/index.html')
def index(request):
    return {}


@render_to('news/detail.html')
def detail(request, pk, slug):
    news = get_object_or_404(News, pk=pk)
    if news.get_absolute_url() != request.path_info:
        return redirect(news.get_absolute_url())

    return {
        'news': news
    }

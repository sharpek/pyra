# -*- encoding: utf-8 -*-
from django.contrib import admin

from .models import News


class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'status', 'author', 'date')

    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/website/admin/js/tinymce_setup.js',
        ]

    def queryset(self, request):
        return super(NewsAdmin, self).queryset(request).select_related('user')

admin.site.register(News, NewsAdmin)
